package yrk.com.ua.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class PublishingHouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int publishingHouseId;
    private String publishingHouseName;
    private String city;
    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "publishingHouse"
    )
    private List<Book> book;
}
