package yrk.com.ua.models;

public enum Language {
    Ukrainian,
    Russian,
    English
}
