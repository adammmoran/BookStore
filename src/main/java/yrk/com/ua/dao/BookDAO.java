package yrk.com.ua.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import yrk.com.ua.models.Book;
import yrk.com.ua.models.Genre;

import java.util.List;

public interface BookDAO extends JpaRepository<Book,Integer>{
    List<Book> findAllByGenre(Genre genre);
}
