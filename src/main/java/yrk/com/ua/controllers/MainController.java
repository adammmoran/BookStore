package yrk.com.ua.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import yrk.com.ua.dao.BookDAO;
import yrk.com.ua.models.Book;
import yrk.com.ua.models.Genre;
import yrk.com.ua.models.Language;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    BookDAO bookDAO;

    @GetMapping("/")
    public String main(Model model){

        model.addAttribute("books",bookDAO.findAllByGenre(Genre.Thience));
        return "main";
    }

    @GetMapping("/add")
    public String addB(Model model){
        List<String> genres = new ArrayList<>();
        for (Genre genre : Genre.values()) {
            genres.add(genre.name());
        }
        List<String> languages = new ArrayList<>();
        for (Language language : Language.values()) {
            languages.add(language.name());
        }
        model.addAttribute("genres",genres);
        model.addAttribute("languages",languages);
        return "add";
    }
    @PostMapping("/addBook")
    public String regUser(Book book){
        bookDAO.save(book);

        return "redirect:/add";
    }

    @GetMapping("/book/{id}")
    public String bookId(@PathVariable("id")int id, Model model){
        model.addAttribute("book",bookDAO.findOne(id));
        return "book";
    }
}
