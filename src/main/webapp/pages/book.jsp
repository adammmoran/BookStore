<%--
  Created by IntelliJ IDEA.
  User: yaroslav
  Date: 9/19/2018
  Time: 09:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book ${book.bookId}</title>
</head>
<body>
    <h1>${book.bookId}</h1>
    <h1>${book.bookName}</h1>
    <h1>${book.description}</h1>
    <h1>${book.year}</h1>
    <h1>${book.genre}</h1>
</body>
</html>
