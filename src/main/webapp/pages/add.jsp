<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: yaroslav
  Date: 9/19/2018
  Time: 08:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
    <form action="/addBook" method="post" class="form-inline">
        <div class="form-group">
            <label for="bookN">Book name</label>
            <input type="text" name="bookName" class="form-control" id="bookN" placeholder="Book Name">
        </div>
        <div class="form-group">
            <label for="desc">Book name</label>
            <input type="text" name="description" class="form-control" id="desc" placeholder="description">
        </div>
        <div class="form-group">
            <label for="gnr">Genre</label>
            <select name="genre" class="form-control" id="gnr" placeholder="Genre">
                <c:forEach items="${genres}" var="genre" >
                    <option value="${genre}">${genre}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="text" name="img" class="form-control" id="image" placeholder="image">
        </div>
        <div class="form-group">
            <label for="yr">Year</label>
            <input type="text" name="year" class="form-control" id="yr" placeholder="yr">
        </div>
        <div class="form-group">
            <label for="lang">Language</label>
            <select name="language" class="form-control" id="lang" placeholder="language">
                <c:forEach items="${languages}" var="language" >
                    <option value="${language}">${language}</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <input type="submit" value="Register!">
        </div>
    </form>

</body>
</html>
