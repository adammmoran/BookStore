<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: yaroslav
  Date: 9/19/2018
  Time: 06:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main</title>
</head>
<body>
    <c:forEach items="${books}" var="book" >
        <a href = "/book/${book.bookId}">
            <p>${book.bookName}</p>
            <img src="${book.img}">
        </a>
        <hr>
    </c:forEach>
</body>
</html>
